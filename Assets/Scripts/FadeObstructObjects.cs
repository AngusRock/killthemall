﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeObstructObjects : MonoBehaviour {

    public GameObject building = null;
    //private Color colorAlpha;

    void Start()
    {
        //colorAlpha = building.GetComponent<MeshRenderer>().material.color;
    }
    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {            
            SetMaterialTransparent();
            iTween.FadeTo(building, 0.25f, 1);
            //colorAlpha.a = 0.2f;
        }
    }
    
    private void SetMaterialTransparent()    
    {
        foreach (Material m in building.GetComponent<Renderer>().materials)
        {
            m.SetFloat("_Mode", 2);    
            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);    
            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);    
            m.SetInt("_ZWrite", 0);    
            m.DisableKeyword("_ALPHATEST_ON");    
            m.EnableKeyword("_ALPHABLEND_ON");    
            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");    
            m.renderQueue = 3000;
        }    
    }

    private void SetMaterialOpaque()    
    {    
        foreach (Material m in building.GetComponent<Renderer>().materials)    
        {    
            m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);    
            m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);    
            m.SetInt("_ZWrite", 1);    
            m.DisableKeyword("_ALPHATEST_ON");    
            m.DisableKeyword("_ALPHABLEND_ON");    
            m.DisableKeyword("_ALPHAPREMULTIPLY_ON");    
            m.renderQueue = -1;    
        }    
    }
    
    void OnTriggerExit(Collider collider)    
    {    
        if(collider.tag == "Player")    
        {    
            // Set material to opaque    
            iTween.FadeTo(building, 1, 1);
            Invoke("SetMaterialOpaque", 1.0f);    
        }
    }
}