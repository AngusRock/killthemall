﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyScoreAnimation : MonoBehaviour {

    private TextMeshPro PreFab_plScoreTextOnZombieHead;
    private RectTransform PreFab_plScoreZombieHeadRectTrans;
    private bool PreFab_scorePointsZombieHead = false;
    private float posX1, posY1, posZ1, posYToReach1, pointsUpMoveSpeed1;
    // Use this for initialization
	void Start () {
        PreFab_plScoreTextOnZombieHead = GetComponent<TextMeshPro>();
        PreFab_plScoreZombieHeadRectTrans = GetComponent<RectTransform>();		
	}
	
	// Update is called once per frame
	void Update () {
        if (PreFab_scorePointsZombieHead)
        {
            PreFab_ScorePointsZombieHeadWithGrenade();
        }		
	}

    public void ScorePointsZombieTrue(float posX, float posZ, float posY, float posYToReach, float pointsUpMoveSpeed) //Viene de PlayerScore.cs
    {
        posX1 = posX;
        posY1 = posY;
        posZ1 = posZ;
        posYToReach1 = posYToReach;
        pointsUpMoveSpeed1 = pointsUpMoveSpeed;
        //        PreFab_plScoreTextOnZombieHead.alpha = 1f;
        //        PreFab_plScoreTextOnZombieHead.enabled = true;
        PreFab_scorePointsZombieHead = true;
    }

    void PreFab_ScorePointsZombieHeadWithGrenade() //Puntos en la cabeza del zombie. Se accede si es verdadero un bool
    {   
        if (PreFab_plScoreTextOnZombieHead.alpha > 0f) //Voy quitando Alpha para que vaya desapareciendo el cartel
        {
            PreFab_plScoreTextOnZombieHead.alpha -= Time.deltaTime * 0.5f;
        }

        if (posY1 <= posYToReach1) //Moviemiento hacia arriba de los puntos
        {
            posY1 += Time.deltaTime * pointsUpMoveSpeed1 / 2; //Divido por dos para hacer que dure mas tiempo la subida del punto
        }
        else
        {
            PreFab_scorePointsZombieHead = false;
            PreFab_plScoreTextOnZombieHead.enabled = false;
            PreFab_plScoreTextOnZombieHead.alpha = 1f; //Reseteo el alpha
            Destroy(gameObject);
        }
        PreFab_plScoreZombieHeadRectTrans.position = new Vector3(posX1, posY1, posZ1);
    }


}
