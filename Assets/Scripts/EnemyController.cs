﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {

    private NavMeshAgent enemyAgent;
    public Transform playerTarget;
    private bool followPlayer = false;
    private bool followTargets = true;
//  private int targetCounter = 0;
    private List<int> list = new List<int>();
    private int listRandomIndex;
    //EnemyController enemyController;
    private bool enemyAgentSpeedDown = false;
    Rigidbody rigidBody;
    EnemyAnimations enemyAnimations;
    public float extraRotationSpeed;
    Vector3 lookRotation;
    Vector3 zombiePosition; //Esta variable la uso para arreglar el error de "Look rotation viewing is zero".. Fijarse debajo de todo
    private bool runExtraRotation = true;
    private Animator animator;
    public PlayerScore playerScore;
    private ZombieSounds zombieSounds;
    public ZombieWalkSoundGlobal zombieWalkSoundGlobal;
    private BoxCollider boxCollider;
    // Use this for initialization
	void Start () {
        enemyAgent = GetComponent<NavMeshAgent>();
        //enemyController = GetComponent<EnemyController>(); //Nose si es necesario si estoy dentro del mismo script
        rigidBody = GetComponent<Rigidbody>();
        enemyAnimations = GetComponent<EnemyAnimations>();
        animator = GetComponent<Animator>();
        zombieSounds = GetComponent<ZombieSounds>();
        boxCollider = GetComponent<BoxCollider>();
        FillTargetsRandomList();
        SetDestinationToTargets();
    }
	
	// Update is called once per frame
	void Update ()
    {        
        ExtraRotation(); //Hace que el agent rote más rapido

        if (followPlayer && gameObject.layer == 9) //Layer 9 = "EnemyCollision". Esto es para que los Zombies muertos no sigan un target
        {
            FollowPlayer();
        }
        else if (followTargets && gameObject.layer == 9) //Layer 9 = "EnemyCollision". Esto es para que los Zombies muertos no sigan un target
        {
            FollowTargets();
        }

        if (enemyAgentSpeedDown) //Esto lo hago para que el Zombie siga avanzado un poquito al caer por un disparo. Queda mas natural
        {
            enemyAgent.speed -= Time.deltaTime * 4f; 
            if (enemyAgent.speed <= 0f)
            {               
                enemyAgent.enabled = false;
                //enemyController.enabled = false; //Desactivo el script. Esto lo hice porque un par de veces, me tiro error del Remaining Distance por desactivar el Nav Mesh Agent
            }
        }
	}

    public void FollowPlayerTrue() //Se ejecuta desde EnemyTriggerDetection cuando el player entra al trigger
    {
        //if (followPlayer)
        //    return;
        followPlayer = true;
        followTargets = false;
        animator.SetBool("FollowPlayer", true); //Se ejecuta una animación que tiene mas velocidad
        enemyAgent.speed = 2.5f;
        //animator.speed = 3;
        //enemyAgent.SetDestination(playerTarget.position);
        zombieWalkSoundGlobal.ZombieWalkSoundFalse();
        zombieSounds.TurnAroundToAttackSoundTrue();
    }

    public void FollowTargetsTrue() //Se ejecuta desde EnemyTriggerDetection cuando el player entra al trigger
    {
        followPlayer = false;
        followTargets = true;
        animator.SetBool("FollowPlayer", false);
        animator.SetBool("Attack",false);
        enemyAgent.speed = 1;
        zombieWalkSoundGlobal.ZombieWalkSoundTrue();
        zombieSounds.WalkingSound();
    }

    void FollowPlayer()
    {        
        enemyAgent.SetDestination(playerTarget.position);
        zombieSounds.TurnAroundToAttackSound();
    }

    //---------NAV MESH TARGETS RANDOM------------
    void FollowTargets()
    {  
        if (enemyAgent.remainingDistance <= 0.2f + enemyAgent.stoppingDistance)
        {
            SetDestinationToTargets();

            if (list.Count == 0)
            {                
                list.Clear();               
                FillTargetsRandomList();
            }
        }
    }

    void FillTargetsRandomList() //Lista targets que sigue el Zombie
    {
        for (int i = 0; i < EnemyDestinationPoints.GetTargetsCount(); i++)
        {
            list.Add(i);
        }
    }

    void SetDestinationToTargets()
    {
        listRandomIndex = Random.Range(0, list.Count);
        enemyAgent.SetDestination(EnemyDestinationPoints.GetTarget(list[listRandomIndex]));
        list.Remove(list[listRandomIndex]);
    }

    //---------//NAV MESH TARGETS RANDOM------------

    public void GunHit() //Esto se llama desde el PlayerShoot.cs
    {
        boxCollider.enabled = false;
        rigidBody.isKinematic = true;
        runExtraRotation = false; //Evito que corra la funcion ExtraRotation, porque sino sale una advertencia, y el zombie se comporta raro cuando vuelta
        enemyAnimations.DeadAnimation(); //Se encuentra en EnemyAnimations.cs
        followTargets = false;
        followPlayer = false;
        enemyAgentSpeedDown = true;
        gameObject.layer = LayerMask.NameToLayer("EnemyNoCollision");
        gameObject.tag = "ZombieDead";
        zombieSounds.DieSound();
        StartCoroutine(WaitForZombieDieSound());
    }

    public void GrenadeHit() //Esto se llama desde GrenadeExplosion.cs
    {
        //enemyAnimations.DeadAnimation();
        boxCollider.enabled = false;
        StartCoroutine(GrenadeHitIsKinematic()); //Para que cuando los golpea la granada, no se queden girando
        playerScore.ScorePoints(transform);
        runExtraRotation = false; //Evito que corra la funcion ExtraRotation, porque sino sale una advertencia, y el zombie se comporta raro cuando vuela
        enemyAnimations.StopAnimation();
        //enemyAgent.Stop();
        enemyAgent.enabled = false;
        followTargets = false;
        followPlayer = false;
        //StartCoroutine(AddForceToEnemy());
        rigidBody.AddForce(0f, 400f, 0f);
        gameObject.layer = LayerMask.NameToLayer("EnemyNoCollision");
        gameObject.tag = "ZombieDead"; //Le cambio el tag para que no colisione de nuevo con las explosiones de la granada
        zombieSounds.enabled = false;
        zombieWalkSoundGlobal.ZombieWalkSoundTrue();
        zombieSounds.DisableAllSoundsForDeadZombie();
    }

    /*IEnumerator AddForceToEnemy()
    {
        yield return new WaitForSeconds(0.01f);
        rigidBody.AddForce(0f, 400f, 0f);
    }*/

    void ExtraRotation() //Hace que el agent rote más rapido Link: http://answers.unity3d.com/questions/660646/increase-navmesh-rotation-speed.html
    {
        if (runExtraRotation)
        {           
            zombiePosition = new Vector3(transform.position.x + 0.0001f, transform.position.y, transform.position.z); //Le agrego 0.0001f al final, para corregir error "Look rotation viewing is zero"
            //Link con solucion: https://forum.unity3d.com/threads/simple-fix-for-look-rotation-viewing-vector-is-zero.411731/
            lookRotation = enemyAgent.steeringTarget - zombiePosition;
           transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(lookRotation), extraRotationSpeed * Time.deltaTime);
        }
    }

    IEnumerator GrenadeHitIsKinematic()
    {
        yield return new WaitForSeconds(1.5f);
        rigidBody.isKinematic = true;
    }

    IEnumerator WaitForZombieDieSound()
    {
        yield return new WaitForSeconds(1.5f);
        zombieWalkSoundGlobal.ZombieWalkSoundTrue();
        zombieSounds.DisableAllSoundsForDeadZombie();
    }

}
