﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeExplosion : MonoBehaviour {

    public float explosionRadius = 50f;
    public float explosionForce = 400f;
    private Collider[] collidersInsideSphere;
    public EnemyAnimations enemy;
    public GameObject explosion;

    void OnCollisionEnter(Collision otherObject)
    {       
//        collidersInsideSphere = Physics.OverlapSphere (Vector3.zero, explosionRadius); //Tomo los collider que entren en la esfera según el radio de explosion
//
//        for (int i = 0; i < collidersInsideSphere.Length; i++) 
//        {
//            Rigidbody rb = collidersInsideSphere[i].attachedRigidbody; //Esto le pasa el rigidbody de los Colliders que estan dentro de colliderInSphere
//            if (rb != null && rb.gameObject.tag != "Player") //Si el collider de arriba tiene un rigidbody y el gameobject no es la granda, entonces..
//            {
//                if (rb.gameObject.tag == "Zombie") //Este if ahora esta al pedo, pero es para cuando tambien exploten barriles
//                {
//                    rb.gameObject.GetComponent<EnemyController>().GrenadeHit();
//                }
//                rb.AddExplosionForce(explosionForce, transform.position, explosionRadius);
//            }
//        }
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }

}
