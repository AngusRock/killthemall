﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerWin : MonoBehaviour {

    public Transform enemyQuantity;
    public TextMeshProUGUI youWinText;
    public ButtonControllerInGame buttonControllerInGame;
    private PlayerController playerController;
    public CanvasTimer canvasTimer;
    int numberOfEnemies = 0;
    public TextMeshProUGUI zombieCounter;
    // Use this for initialization
	void Start () {
        numberOfEnemies = enemyQuantity.childCount;
        playerController = GetComponent<PlayerController>();
        //print(enemyQuantity.childCount);
        zombieCounter.text = "Zombies: "+ "<color=#ff0000>"+numberOfEnemies.ToString()+"</color>";
	}
	
	// Update is called once per frame
	void Update () {
    }

    public void EnemyDeadCountDown() //Viene de PlayerScore.cs
    {
        numberOfEnemies--;
        zombieCounter.text = "Zombies: "+ "<color=#ff0000>"+numberOfEnemies.ToString()+"</color>";
        if (numberOfEnemies <= 0)
        {
            canvasTimer.StopTimer();
            youWinText.enabled = true;
            playerController.ControllerOff();
            StartCoroutine(PlayAgainQuestionMenu());
        }
    }

    IEnumerator PlayAgainQuestionMenu()
    {
        yield return new WaitForSeconds(2.5f);
        buttonControllerInGame.PlayAgainQuestionMenu();
    }
}
