﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour {

    Rigidbody rigidBody;
    float magnitude = 40f;
    Vector3 force;
    PlayerLife playerLife;
    bool waitForNextHit = false;
    // Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        playerLife = GetComponent<PlayerLife>();
	}

    void OnCollisionStay(Collision otherObject) //Uso OnCollisionStay porque da mejor resultado para revolear al Player
    {
        if (otherObject.transform.tag == "Zombie") 
        {  
            //---Para que rebolee el jugador hacia atrás, no importa de la posición que venga----
            //Link: http://answers.unity3d.com/questions/1100879/push-object-in-opposite-direction-of-collision.html
            force = transform.position - otherObject.transform.position;
            force.Normalize();
            rigidBody.AddForce((force * magnitude) + (Vector3.up * 30)); //Le agregué un poco de fuerza hacia arriba tambien
        }
        //Link: http://answers.unity3d.com/questions/1100879/push-object-in-opposite-direction-of-collision.html
        //Link: http://answers.unity3d.com/questions/311247/player-pushback-when-collide-with-enemy.html
    }

    void OnCollisionEnter(Collision otherObject)
    {
        if (otherObject.transform.tag == "Zombie") 
        {
            if (!waitForNextHit) //Con esto le agrego un pequeña espera a la quita de vidas, porque a veces le saca muchas vidas de una y nose porque
            {
                playerLife.PlayerLifeText();
                StartCoroutine(WaitForNextHit());
            }
        }
    }

    IEnumerator WaitForNextHit()
    {
        waitForNextHit = true;
        yield return new WaitForSeconds(0.5f);
        waitForNextHit = false;
    }
}
