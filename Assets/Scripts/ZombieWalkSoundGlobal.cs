﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieWalkSoundGlobal : MonoBehaviour {

    //Este script es para desactivar el walk sound de todos los zombies, cuando esta sonando el attack sound
    //Es porque quedan raros los dos juntos
    public bool zombieWalkSound = true;
    public void ZombieWalkSoundFalse()
    {
        zombieWalkSound = false;
    }
    public void ZombieWalkSoundTrue()
    {
        zombieWalkSound = true;
    }

}
