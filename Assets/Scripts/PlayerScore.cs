﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerScore : MonoBehaviour {

    public TextMeshProUGUI plScoreCanvas;
    private int scoreCounter;
    private int scoreZombiePoints = 5;
    public float pointsUpMoveSpeed = 3.31f;
    private float posYUnitsToAdd = 1.5f;
    private float posY = -0.229f; //Este valor es solo para testear cuando presiono T
    private float posX = 6.3f; //Este valor es solo para testear cuando presiono T
    private float posZ = -14.83f; //Este valor es solo para testear cuando presiono T
    private float posYToReach;
    public GameObject plScoreOnZombieHead; //Para el Prefab
    private GameObject plScoreOnZombieHeadInst;
    private EnemyScoreAnimation zombieScoreAnimation; //Esto esta dentro del prefab del puntaje que se instancia
    private PlayerWin playerWin;

    // Use this for initialization
	void Start () {
        playerWin = GetComponent<PlayerWin>();
	}
	
	// Update is called once per frame
	void Update () {
        ///--------PARA TESTEAR EL SCORE.. AHORA NO FUNCIONARíA!!!--------
        /*if (Input.GetKeyDown(KeyCode.T))
        {
            //Instantiate(scorePL, transform.position, Quaternion.identity);
            posY = -0.229f + 2.08f;
            posYToReach = posY + posYUnitsToAdd;
            plScoreOnZombieHead.alpha = 1f;
            plScoreOnZombieHead.enabled = true;
            scorePointsZombieHead = true;
            plScoreOnZombieHead.text = scoreZombiePoints.ToString();
        }*/
        ///--------//PARA TESTEAR EL SCORE--------
	}

    public void ScorePoints(Transform objectHit)
    {        
        posX = objectHit.transform.position.x; //Tomo la posición del Zombie
        posZ = objectHit.transform.position.z; //Tomo la posición del Zombie
        posY = objectHit.transform.position.y + 2.08f; //Tomo la posición del Zombie, y le agrego 2.08 unidades en Y para que se separe más de la cabeza
        posYToReach = posY + posYUnitsToAdd; //Asigno cuantas unidades debería subir el texto
        switch (objectHit.transform.tag)
        {
            case "Zombie":
                ScoreZombieInstantiate();
                break;            
        }
        plScoreCanvas.text = "Puntos: " + scoreCounter.ToString();
        //PreFab_plScoreTextOnZombieHead.text = scoreZombiePoints.ToString();
        playerWin.EnemyDeadCountDown();
    }

    void ScoreZombieInstantiate()
    {
        scoreCounter += scoreZombiePoints;
        plScoreOnZombieHeadInst = Instantiate(plScoreOnZombieHead, transform.position, Quaternion.identity);
        zombieScoreAnimation = plScoreOnZombieHeadInst.gameObject.GetComponent<EnemyScoreAnimation>();
        zombieScoreAnimation.ScorePointsZombieTrue(posX, posZ, posY, posYToReach, pointsUpMoveSpeed);
    }
}
