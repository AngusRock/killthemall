﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDestinationPoints : MonoBehaviour 
{
    private static EnemyDestinationPoints myInstance;

    private List<Vector3> targetPoints = new List<Vector3>();

    void Awake ()
    {
        if (myInstance == null)
            myInstance = this;

        for (int i = 0; i < transform.childCount; i++)
        {
            targetPoints.Add(transform.GetChild(i).position);

        }
	}
	
    public static Vector3 GetTarget(int i)
    {
        return myInstance.targetPoints[i];
    }

    public static int GetTargetsCount()
    {
        return myInstance.targetPoints.Count;
    }
}
