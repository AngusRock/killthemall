﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public GameObject player;
    private Vector3 offset;
    private float zPosMin = -46.44f;
    private float zPosMax = 46.44f;
    bool follow = true;

    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    void LateUpdate () {
        if (follow)
        {
            transform.position = player.transform.position + offset;
            transform.position = new Vector3(transform.position.x,transform.position.y,Mathf.Clamp(transform.position.z,zPosMin,zPosMax));
        }

    }

    public void FollowTrueFalse()
    {
        follow = !follow;
        offset = transform.position - player.transform.position;
        print(follow);
    }
}

