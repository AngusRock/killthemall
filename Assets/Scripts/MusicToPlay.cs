﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicToPlay : MonoBehaviour {

    private AudioSource audioSource;
    public AudioClip audioClip;
    // Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = audioClip;

	}

    public void PlayMusic()
    {
        audioSource.time = 7.7f;
        audioSource.Play();
    }
}
