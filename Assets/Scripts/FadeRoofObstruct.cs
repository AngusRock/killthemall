﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeRoofObstruct : MonoBehaviour {

    public GameObject roof;
    private List<Renderer> roofList = new List<Renderer>();
    bool checkIfAlphaOn = false; //Esto lo puse, porque el FadeRoofObstructReset.cs ejecuta RestoreRoofMaterialAlpha() que tiene un Invoke con 1seg de retardo
    //y esto hacia quilombo cuando se quería activar el alpha, porque luego de ese segundo se desactivaba de nuevo

    void Awake()
    {
        for (int i = 0; i < roof.transform.childCount; i++)
        {
            //test.Add(gameObject.GetComponentInChildren<Renderer>().material);
            roofList.Add(roof.transform.GetChild(i).GetComponentInChildren<Renderer>());
        }
    }

//    void Start()
//    {
//        SetRoofMaterialTransparent();
//        for (int i = 0; i < roofList.Count; i++)
//        {
//            iTween.FadeTo(roofList[i].gameObject, 0, 1);
//        }
//    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")
        {
            checkIfAlphaOn = true;
            //SetMaterialTransparent();
            SetRoofMaterialTransparent();
            for (int i = 0; i < roofList.Count; i++)
            {
                iTween.FadeTo(roofList[i].gameObject, 0.15f, 1);
            }
        }
    }

    private void SetRoofMaterialTransparent()    
    {
        for (int i = 0; i < roofList.Count; i++)
        {
            foreach (Material m in roofList[i].materials)
            {
                m.SetFloat("_Mode", 2);    
                m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);    
                m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);    
                m.SetInt("_ZWrite", 0);    
                m.DisableKeyword("_ALPHATEST_ON");    
                m.EnableKeyword("_ALPHABLEND_ON");    
                m.DisableKeyword("_ALPHAPREMULTIPLY_ON");    
                m.renderQueue = 3000;    
            }
        }
            
    }

    private void SetRoofMaterialOpaque()    
    {
        for (int i = 0; i < roofList.Count; i++)
        {
            foreach (Material m in roofList[i].materials)
            {    
                m.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);    
                m.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);    
                m.SetInt("_ZWrite", 1);    
                m.DisableKeyword("_ALPHATEST_ON");    
                m.DisableKeyword("_ALPHABLEND_ON");    
                m.DisableKeyword("_ALPHAPREMULTIPLY_ON");    
                m.renderQueue = -1;    
            }
        }
    }

    public void RestoreRoofMaterialAlpha()    
    {     
        // Set material to opaque
        if (checkIfAlphaOn)
        {
            for (int i = 0; i < roofList.Count; i++)
            {
                iTween.FadeTo(roofList[i].gameObject, 1, 1);
            }
            checkIfAlphaOn = false;
            Invoke("SetRoofMaterialOpaque", 1.0f);
        }
    }
}