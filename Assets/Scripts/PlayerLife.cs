﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerLife : MonoBehaviour {

    public TextMeshProUGUI plLifeCanvas;
    public TextMeshPro plLifeOnHead;
    public RectTransform plLifeOnHeadRectTrans;
    public int lifeCounter = 7;
    float posX;
    float posY;
    float posZ;
    float posYToReach;
    float posYUnitsToAdd = 1f;
    public float lifeUpMoveSpeed = 3.31f;
    bool playerLifeOnHead = false;
    int lifeUnitsToSubtract = 1;
    public TextMeshProUGUI plLoseText;
    private PlayerController playerController;
    public ButtonControllerInGame buttonControllerInGame;
    private PlayerRagdollEnable playerRagdollEnable;
    // Use this for initialization
	void Start () {
        plLifeCanvas.text = "Vidas: " + lifeCounter.ToString();
        playerController = GetComponent<PlayerController>();
        playerRagdollEnable = GetComponent<PlayerRagdollEnable>();
	}
	
	// Update is called once per frame
	void Update () {
        if (playerLifeOnHead)
        {
            PlayerLifeOnHead();
        }
	}

    public void PlayerLifeText()
    {
        plLifeOnHead.alpha = 1f; //Reseteo el alpha desde acá arriba, porque si disparo de nuevo muy rapido, el IF de PosY PlayerLifeOnHead() y alpha no se resetea
        posY = transform.position.y + 2.08f;
        posYToReach = posY + posYUnitsToAdd; //Asigno cuantas unidades debería subir el texto
        lifeCounter -= lifeUnitsToSubtract;
        if (lifeCounter > 0)
        {            
            playerLifeOnHead = true;     //Pongo en true para que se llame a PlayerLifeOnHead() desde el Update()
            plLifeOnHead.enabled = true; //Activo el TextMeshPro para que se visualice el texto
            plLifeCanvas.text = "Vidas: " + lifeCounter.ToString();
            plLifeOnHead.text = "-1";
        }
        else //Player esta muerto
        {
            plLifeCanvas.text = "Vidas: 0";
            gameObject.tag = "PlayerDead";
            playerLifeOnHead = true;     //Pongo en true para que se llame a PlayerLifeOnHead() desde el Update()
            plLifeOnHead.enabled = true; //Activo el TextMeshPro para que se visualice el texto
            plLifeOnHead.text = "-1";
            plLoseText.enabled = true;
            playerController.ControllerOff();
            playerRagdollEnable.EnableRagdoll();
            StartCoroutine(TryAgainQuestionMenuCRoutine()); //Para que se muestre el menu Try Again, luego de unos segundos
        }

    }

    void PlayerLifeOnHead() //Puntos en la cabeza del zombie
    {   
        posX = transform.position.x; //Esto va aca, para que el texto siga la posición del player
        posZ = transform.position.z; //Esto va aca, para que el texto siga la posición del player

        if (plLifeOnHead.alpha > 0f) //Voy quitando Alpha para que vaya desapareciendo el cartel
        {
            plLifeOnHead.alpha -= Time.deltaTime * 0.5f; //Antes decía "2f".. Hacia que fuera mas rapido
        }

        if (posY <= posYToReach) //Moviemiento hacia arriba de los puntos
        {
            posY += Time.deltaTime * lifeUpMoveSpeed / 2;
        }
        else
        {
            playerLifeOnHead = false;
            plLifeOnHead.enabled = false;
            plLifeOnHead.alpha = 1f; //Reseteo el alpha
        }
        plLifeOnHeadRectTrans.position = new Vector3(posX, posY, posZ);
    }

    IEnumerator TryAgainQuestionMenuCRoutine()
    {
        yield return new WaitForSeconds(2.5f);
        buttonControllerInGame.TryAgainQuestionMenu();
    }
}
