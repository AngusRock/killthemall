﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimations : MonoBehaviour {

    private Animator animator;
    // Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DeadAnimation() //Esto se invoca cuando el raycast del player le pega
    {
        animator.Play("Dead_BackFall");
    }
    public void StopAnimation() //Esto se invoca cuando la granada le pega
    {
        animator.Stop();
    }

    public void AttackAnimation() //Esto se invoca cuando el raycast del player le pega
    {
        animator.Play("Dead_BackFall");
    }
}
