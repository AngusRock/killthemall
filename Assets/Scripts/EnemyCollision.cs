﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollision : MonoBehaviour {

    Rigidbody rigidBody;
    Animator animator;
    private ZombieSounds zombieSounds;
    public ZombieWalkSoundGlobal zombieWalkSoundGlobal;
	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        zombieSounds = GetComponent<ZombieSounds>();
	}

    void OnCollisionEnter(Collision otherObject)
    {
        if (otherObject.transform.tag == "Player")            
        {
            rigidBody.isKinematic = true; //Esto lo hago para que el player no pueda empujar al Zombie y que haga cualquier cosa
            animator.SetBool("Attack",true);
            zombieWalkSoundGlobal.ZombieWalkSoundFalse();
            zombieSounds.AttackSound();
        }
    }

    void OnCollisionExit(Collision otherObject)
    {
        if (otherObject.transform.tag == "Player")            
        {
            rigidBody.isKinematic = false;
            animator.SetBool("Attack",false);
            //zombieWalkSoundGlobal.ZombieWalkSoundTrue(); //Esto se pasa a True desde EnemyController.cs, cuando se hace FollowTargetTrue, y otros mas
        }
    }
}
