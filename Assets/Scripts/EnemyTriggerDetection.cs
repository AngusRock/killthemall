﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTriggerDetection : MonoBehaviour {

	// Use this for initialization
    public EnemyController enemyController;
	void Start () {
        //enemyController = GetComponent<EnemyController>();
        //enemyController = FindObjectOfType(typeof(EnemyController)) as EnemyController;
	}
	
	// Update is called once per frame
	void Update () {		
	}

    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.gameObject.tag == "Player")
        {           
            enemyController.FollowPlayerTrue();
        }
        else if (otherObject.gameObject.tag == "PlayerDead")
        {           
            enemyController.FollowTargetsTrue();
        }
    }

    void OnTriggerExit(Collider otherObject)
    {
        if (otherObject.gameObject.tag == "Player")
        {           
            enemyController.FollowTargetsTrue();
        }
    }

    void OnTriggerStay(Collider otherObject)
    {
        if (otherObject.gameObject.tag == "PlayerDead") //Esto lo hice, porque sino continuaban siguiendo al player muerto
        {           
            enemyController.FollowTargetsTrue();
        }
    }
}
