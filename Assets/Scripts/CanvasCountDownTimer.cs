﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CanvasCountDownTimer : MonoBehaviour {

    public TextMeshProUGUI countDownTimer;
    public RectTransform countDownTimerTransf;
    int counterValue = 3;
    int counter;
    float originalFontSize = 90;
    bool showCountDown = true;
    public GameObject instructionsMenu;
    bool runOnlyOnce = true;
    bool showKillThemAll = false;
    public MusicToPlay musicToPlay;
    public bool zombieSoundsEnabled = false;//Esto lo pongo acá, porque si lo hiciera en el script de ZombieSounds.cs, tendría que aplicar el cambio en el script que tiene asignado cada Zombie
    public PlayerController playerController;
    private ButtonControllerInGame buttonControllerInGame;
    private CanvasTimer canvasTimer;
    // Use this for initialization
	void Start () {
        countDownTimer.fontSize = originalFontSize;
        counter = counterValue;
        buttonControllerInGame = GetComponent<ButtonControllerInGame>();
        canvasTimer = GetComponent<CanvasTimer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (showCountDown && instructionsMenu.activeSelf == false)
        {
            CountDown();
        }
        if (showKillThemAll)
        {
            KillThemAll();
        }
	}

    void CountDown()
    {
        if (runOnlyOnce) //Esto es para que muestre el primer valor del contador antes de restarlo. Si lo pongo en el IF debajo,
                          //se nota como cambia el numero cuando el Size de la fuente vuelve al original, y queda mal.
        {
            countDownTimer.text = counter.ToString();
            runOnlyOnce = false;
        }       
        //countDownTimer.fontSize -= 1.9f;
        countDownTimer.fontSize -= Time.deltaTime * 50f;
        //countDownTimer.fontSize -= (Time.unscaledTime / 1.8f); //Ahora funcioan!! debia ser algun cuelgue de unity!
        countDownTimer.alpha -= Time.deltaTime * 1.05f;
        //countDownTimer.alpha -= 0.03f; //Lo hago asi (sin usar Time.deltaTime) porque el time.scale esta en cero mientras resta alpha y Time.deltaTime no funciona
        //Se podría usar el time.unscaledDeltaTime, pero arranca mal. No inicia desde cero (es como que pasan unos segundos y luego impacta en el codigo)
        //countDownTimer.alpha -= (Time.unscaledTime * 0.01f); //Ahora funcioan!! debia ser algun cuelgue de unity!
        
        if (countDownTimer.fontSize <= 40f)
        {            
            countDownTimer.fontSize = originalFontSize;
            countDownTimer.alpha = 1f;
            counter--;
            countDownTimer.text = counter.ToString();
            if (counter <= 0)
            {          
                showCountDown = false;
                showKillThemAll = true;
                runOnlyOnce = true;
            }                
        }

    }

    void KillThemAll()
    {
        if (runOnlyOnce)
        {
            countDownTimer.fontSize = 130f;
            countDownTimer.color = Color.red;
            countDownTimer.text = "KIll them All!";
            Time.timeScale = 1f;
            runOnlyOnce = false;
            musicToPlay.PlayMusic();
            zombieSoundsEnabled = true;
            playerController.ControllerOn();
            buttonControllerInGame.EnableEscKey();
            canvasTimer.StartTimer();
        }
        //countDownTimer.fontSize -= 1.3f;
        countDownTimer.fontSize -= Time.deltaTime * 40f;
        //countDownTimer.fontSize -= (Time.unscaledTime / 3.5f);//Ahora funcioan!! debia ser algun cuelgue de unity!
        //countDownTimer.alpha -= 0.020f;
        countDownTimer.alpha -= Time.deltaTime * 0.7f;
        //countDownTimer.alpha -= (Time.unscaledTime * 0.004f);//Ahora funcioan!! debia ser algun cuelgue de unity!
        if (countDownTimer.alpha <= 0f)
        {
            showKillThemAll = false;
        }
    }
}
