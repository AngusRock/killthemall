﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour {

    private float moveX;
    private float moveY;
    private float moveZ;
    public float speed = 4f;
    private Vector3 movement;
    public GameObject granada;
    public int grenadeQuantity = 4;
    public Transform granadaPosition;
    private GameObject granadaInst;

    public float grenadeForwardForce = 300f;
    public float grenadeUpForce = 100f;
    public TextMeshProUGUI grenadeCounter;
    private Animator animator;
    //private Rigidbody rigidBody;

    RaycastHit hit;
    private bool shoot = false;   
    private float timer = 0.1f;
    private float timerCurrent;
    public ParticleSystem particlesGunBullets;
    public ParticleSystem particlesGunRunningPlayer;
    public ParticleSystem particlesGunIdlePlayer;
    public Transform raycastOrigin;
    private PlayerScore playerScore;
    public Transform gunBulletsPosIdlePlayer; //Establece la posición de las particulas de las balas, si el player esta quieto
    public Transform gunBulletsPosRunningPlayer; //Establece la posición de las particulas de las balas, si el player esta corriendo
    private bool gunBulletsAnimation = true;
    //private ParticleSystem particlesGunBulletsInst;

    float previousEulerAnglesY;
    float currentEulerAnglesY;
    private bool controllerOn = false;

    private float zPosMin = -39f;
    private float zPosMax = 30f;
    private float xPosMin = -30f;
    private float xPosMax = 55f;

    public Transform cameraTransf;
    public PlayerGunShotSound playerGunShotSound;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        //rigidBody = GetComponent<Rigidbody>();
        playerScore = GetComponent<PlayerScore>();
        timerCurrent = 0f; //Para que arranque enseguida a disparar
        grenadeCounter.text = "Granadas: " + grenadeQuantity.ToString();
	}
	
	// Update is called once per frame
	void Update ()
    {        
        if (controllerOn)
        {
            PlayerShooting();
            PlayerMovement();
        }
	}

    void FixedUpdate()
    {
        PlayerRaycastShooting();
    }

    public void ControllerOn() //Esto es para que cuando hago click en el boton Ok de la instrucciones, el player no meta un tiro al arrancar el juego
    {
        controllerOn = true;
    }

    public void ControllerOff()
    {
        controllerOn = false;
        animator.SetBool("Run",false);
        animator.SetBool("Shoot", false);
        particlesGunBullets.Stop();
        particlesGunRunningPlayer.Stop();
        particlesGunIdlePlayer.Stop();
    }

    void PlayerMovement()
    {
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, xPosMin, xPosMax), transform.position.y, Mathf.Clamp(transform.position.z, zPosMin, zPosMax));
        moveX = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        moveZ = Input.GetAxis("Vertical") * Time.deltaTime * speed;
        movement = new Vector3(moveX, 0f, moveZ);
        if (movement != Vector3.zero)
        {
            //Quaternion.Slerp hace que gire hacia su espalda, si cambio para la dirección contraria
            animator.SetBool("Run", true);
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(movement), 0.3f); //El ultimo parametro es la velocidad de giro
            transform.Translate(movement, Space.World);
            //transform.Translate(movement, cameraTransf);
            //transform.localPosition += movement * Time.deltaTime;
        }
        else
        {
            animator.SetBool("Run",false);
        }
        //print(rigidBody.velocity.x);

        //Link: http://answers.unity3d.com/questions/803365/make-the-player-face-his-movement-direction.html
        //Link: otra forma, pero sin usar GetAxis: http://answers.unity3d.com/questions/616195/how-to-make-an-object-go-the-direction-it-is-facin.html
        //transform.position += Vector3.right * moveX;
        //transform.position += Vector3.forward * moveZ;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            
            if (grenadeQuantity > 0)
            {
                grenadeQuantity--;
                grenadeCounter.text = "Granadas: " + grenadeQuantity.ToString();
                granadaInst = Instantiate(granada, granadaPosition.position ,Quaternion.identity);
                granadaInst.GetComponent<Rigidbody>().AddForce(transform.forward * grenadeForwardForce + Vector3.up * grenadeUpForce);
            }
            else
            {
                grenadeCounter.text = "Granadas: " + 0.ToString();
            }
        }
    }

    void PlayerShooting()
    { 
        if (Input.GetKey(KeyCode.E) || Input.GetMouseButton(0))
        {            
            if (gunBulletsAnimation) //Para que ponga Play una vez sola. Si lo hace todo el tiempo en intervalos muy cortos, no funciona
            {
                particlesGunBullets.Play();
                gunBulletsAnimation = false;                
            }
            timerCurrent -= Time.deltaTime;
            if (timerCurrent <= 0f)
            {                
                shoot = true;
                timerCurrent = timer;

                if (movement != Vector3.zero) //Si el jugador se esta moviendo, entonces...
                {
                    particlesGunBullets.transform.position = gunBulletsPosRunningPlayer.transform.position;
                    particlesGunBullets.transform.forward = gunBulletsPosRunningPlayer.transform.forward;
                    particlesGunRunningPlayer.Play();
                    particlesGunIdlePlayer.Stop();
                    animator.SetBool("Shoot", false); //Detengo animación que pone en posición de disparo al player
                }
                else
                {
                    particlesGunBullets.transform.position = gunBulletsPosIdlePlayer.transform.position;
                    particlesGunBullets.transform.forward = gunBulletsPosIdlePlayer.transform.forward;
                    particlesGunRunningPlayer.Stop();
                    particlesGunIdlePlayer.Play();
                    animator.SetBool("Shoot", true); //Esta animación pone en posición de disparo al player
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.E) || Input.GetMouseButtonUp(0))
        {
            timerCurrent = 0f; //Para que arranque enseguida a disparar
            particlesGunBullets.Stop();
            gunBulletsAnimation = true; //Para que vuelva a entrar al IF y ponga Play en esta particula
            particlesGunRunningPlayer.Stop();
            particlesGunIdlePlayer.Stop();
            animator.SetBool("Shoot", false);
        }
    }

    void PlayerRaycastShooting()
    {
        if (shoot)
        {            
            playerGunShotSound.GunShotSound();
            //if (Physics.Raycast (raycastOrigin.transform.position, raycastOrigin.transform.forward, out hit, 9.4f))
            if(Physics.SphereCast(raycastOrigin.transform.position, 0.45f, raycastOrigin.transform.forward, out hit, 9.4f)) //El 2do parametro es el radio de la esfera (para facilitar el tiro); el ultimo la distancia maxima del cast
            {
                //print ("Pegó en el objeto: " + hit.transform.name);
                if (hit.transform.tag == "Zombie")
                {                    
                    //enemyController.GunHit();
                    playerScore.ScorePoints(hit.transform);
                    hit.transform.SendMessage("GunHit"); //Se encuentra en EnemyController.cs
                }
                //Debug.DrawLine(raycastOrigin.transform.position, hit.point, Color.yellow);
            }
            shoot = false;
            //gunHit = false;
        } 
    }
}
