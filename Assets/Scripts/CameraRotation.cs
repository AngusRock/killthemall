﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour {

    public Transform cameraRotation;
    public Transform from;
    public Transform to;
    bool rotateOn = false;
    public GameObject player;
    public CameraFollow cameraFollow;
    float rotationCounter = 0;
    float positionCounterX = 0;
    float positionCounterZ = 0;

    float rotationUnits = 21f;
    float positionUnitsX = 3f;
    float positionUnitsZ = 2.8f;
    // Use this for initialization
	void Start () {
        cameraFollow.FollowTrueFalse();
        rotateOn = true;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        if (rotateOn)
        {
            //cameraRotation.rotation = Quaternion.Slerp(from.rotation, to.rotation, 0.01f * Time.deltaTime);
            //cameraRotation.eulerAngles = new Vector3(cameraRotation.eulerAngles.x, -40f, cameraRotation.eulerAngles.z);
            //cameraRotation.position = player.position;
            if (rotationCounter >= -90f)
            {
                cameraRotation.rotation = Quaternion.Euler(cameraRotation.transform.eulerAngles.x, (cameraRotation.transform.eulerAngles.y -rotationUnits * Time.deltaTime), cameraRotation.transform.eulerAngles.z);
                rotationCounter -= rotationUnits * Time.deltaTime;
            }
            if (positionCounterX <= 12.9f)
            {
                cameraRotation.position = new Vector3(cameraRotation.position.x + positionUnitsX * Time.deltaTime, cameraRotation.position.y, cameraRotation.position.z);
                positionCounterX += positionUnitsX * Time.deltaTime;
            }
            if (positionCounterZ <= 12f)
            {
                cameraRotation.position = new Vector3(cameraRotation.position.x, cameraRotation.position.y, cameraRotation.position.z + positionUnitsZ * Time.deltaTime);
                positionCounterZ += positionUnitsZ * Time.deltaTime;
            }
            //cameraRotation.RotateAround(player.transform.position,cameraRotation.up, -10f * Time.deltaTime);
            if (rotationCounter <= -90f)
            {
                cameraFollow.FollowTrueFalse();
                rotateOn = false;
            }
        }

	}

    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.tag == "Player")
        {
            cameraFollow.FollowTrueFalse();
            rotateOn = true;
        }
    }
}
