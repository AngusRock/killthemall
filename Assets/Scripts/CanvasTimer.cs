﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasTimer : MonoBehaviour {

    public TextMeshProUGUI gameTimer;
    public TextMeshProUGUI plLoseText;
    float timerCounter = 120.9f;
    public int timerCounterInteger;
    public PlayerController playerController;
    private ButtonControllerInGame buttonControllerInGame;
    bool timerOn = false;
    // Use this for initialization
	void Start () {
        timerCounterInteger = (int)timerCounter;
        buttonControllerInGame = GetComponent<ButtonControllerInGame>();
	}
	
	// Update is called once per frame
	void Update () {
        if (timerOn)
        {
            TimeCounter();
        }
	}

    public void StartTimer()
    {
        timerOn = true;
    }

    public void StopTimer()
    {
        timerOn = false;
    }

    void TimeCounter()
    {
        gameTimer.text = timerCounterInteger.ToString();
        timerCounter -= Time.deltaTime;
        timerCounterInteger = (int)timerCounter;
        if (timerCounterInteger <= 15)
        {
            gameTimer.color = Color.red;   
        }
        if (timerCounterInteger <= 0)
        {
            gameTimer.text = 0.ToString();
            playerController.ControllerOff();
            StartCoroutine(TryAgainQuestionMenuCRoutine());
            plLoseText.enabled = true;
            plLoseText.text = "Time Out!";
        }
    }

    IEnumerator TryAgainQuestionMenuCRoutine()
    {
        yield return new WaitForSeconds(2.5f);
        buttonControllerInGame.TryAgainQuestionMenu();
    }
}
