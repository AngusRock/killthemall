﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLight : MonoBehaviour {

	// Use this for initialization
    Light luz;
    float randomNumber;
	void Start () {
        luz = GetComponent<Light>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        randomNumber = Random.value;
        if (randomNumber <= 0.9f)
        {
            luz.enabled = true;
        }
        else
        {
            luz.enabled = false;
        }
	}
}
