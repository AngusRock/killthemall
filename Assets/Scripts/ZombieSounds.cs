﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieSounds : MonoBehaviour {

    private AudioSource audioSource;
    public AudioClip walkingSound;
    public AudioClip[] turnAroundToAttackSound;
    public AudioClip[] attackSound;
    public AudioClip dieSound;
    bool playWalkingSound = true;
    bool playTurnAroundToAttackSound = true;
    bool playAttackSound = true;
    bool stopWalkingSound = false;//Esto es para que el WaitForSeconds no ejecute el audioClip que se reproduciría luego de X segundos, si justo se hizo un cambio de audioClip por otro, antes de transcurrido ese tiempo 
    bool stopTurnAroundToAttackSound = false;//Esto es para que el WaitForSeconds no ejecute el audioClip que se reproduciría luego de X segundos, si justo se hizo un cambio de audioClip por otro 
    int i = 0;
    int i2 = 0;
    public CanvasCountDownTimer canvasCountDownTimer;
    public ZombieWalkSoundGlobal zombieWalkSoundGlobal;
    bool disableAllSoundsForDeadZombie = false;
    // Use this for initialization
	void Start ()
    {
        audioSource = GetComponent<AudioSource>();
	}

    void Update()
    {
        if (canvasCountDownTimer.zombieSoundsEnabled && !disableAllSoundsForDeadZombie)
        {
            if (playWalkingSound && zombieWalkSoundGlobal.zombieWalkSound)
            {
                playWalkingSound = false;
                WalkingSound();
            }
            /*if (playTurnAroundToAttackSound)
            {
                playTurnAroundToAttackSound = false;
                TurnAroundToAttackSound();
            }*/
        }
    }

    public void DisableAllSoundsForDeadZombie()
    {
        disableAllSoundsForDeadZombie = true;
    }


    public void WalkingSound()
    {   
        if (!disableAllSoundsForDeadZombie)
        {
            stopWalkingSound = false;
            stopTurnAroundToAttackSound = true;
            audioSource.clip = walkingSound;
            audioSource.Play();             
            StartCoroutine(WaitForWalkingSound());
        }
    }

    IEnumerator WaitForWalkingSound()
    {
        yield return new WaitForSeconds(2f);
        if (!stopWalkingSound)
        {
            playWalkingSound = true;
        }
    }

    public void TurnAroundToAttackSound()
    {   
        if (!disableAllSoundsForDeadZombie && playTurnAroundToAttackSound)
        {            
            playTurnAroundToAttackSound = false;
            stopWalkingSound = true;
            stopTurnAroundToAttackSound = false;
            audioSource.clip = turnAroundToAttackSound[i];
            i++;
            if (i > turnAroundToAttackSound.Length-1)
            {
                i = 0;
            }              
            audioSource.Play();
            StartCoroutine(WaitForTurnAroundToAttackSound());
        }
    }

    public void TurnAroundToAttackSoundTrue()
    {
        playTurnAroundToAttackSound = true;
    }

    IEnumerator WaitForTurnAroundToAttackSound()
    {
        yield return new WaitForSeconds(2.5f);
        if (!stopTurnAroundToAttackSound)
        {
            playTurnAroundToAttackSound = true;
        }
    }

    public void AttackSound()
    {
        if (!disableAllSoundsForDeadZombie && playAttackSound)
        {
            //stopTurnAroundToAttackSound = true;
            //stopWalkingSound = true;
            audioSource.clip = attackSound[i2];
            i2++;
            if (i2 > attackSound.Length-1)
            {
                i2 = 0;
            }
            audioSource.Play();
            //playAttackSound = false;
            //StartCoroutine(WaitForAttackSound());
        }
    }

    IEnumerator WaitForAttackSound()
    {
        yield return new WaitForSeconds(1.5f);
        playAttackSound = true;
    }

    public void DieSound()
    {
        if (!disableAllSoundsForDeadZombie)
        {
            stopTurnAroundToAttackSound = true;
            stopWalkingSound = true;
            audioSource.clip = dieSound; 
            audioSource.Play();
        }  
    }
}
