﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGunShotSound : MonoBehaviour {

    private AudioSource audioSource;
    public AudioClip audioClip;
    // Use this for initialization
	void Start () {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = audioClip;
        audioSource.time = 0.142f;
	}
	
    public void GunShotSound()
    {        
        audioSource.Play();
    }
}
