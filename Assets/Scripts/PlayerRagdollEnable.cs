﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRagdollEnable : MonoBehaviour {

    Collider[] ragdollColliders;
    Rigidbody[] ragdollRigidbodies;
    Collider parentCollider;
    Rigidbody parentRigidbody;
    public GameObject ragdollComponents;
    Animator animator;
    // Use this for initialization
	void Start () {
        ragdollColliders = ragdollComponents.GetComponentsInChildren<Collider>();
        ragdollRigidbodies = ragdollComponents.GetComponentsInChildren<Rigidbody>();
        parentCollider = GetComponent<Collider>();
        parentRigidbody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();

        foreach (Rigidbody rb in ragdollRigidbodies)
        {
            rb.isKinematic = true;
        }
        foreach (Collider col in ragdollColliders)
        {
            col.enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {

        /*if(Input.GetKeyDown(KeyCode.T))
        {
            EnableRagdoll(); 
        }*/
	}

    public void EnableRagdoll()
    {
        animator.enabled = false;
        parentRigidbody.isKinematic = true;
        parentCollider.enabled = false;
        foreach (Rigidbody rb in ragdollRigidbodies)
        {
            rb.isKinematic = false;
        }
        foreach (Collider col in ragdollColliders)
        {
            col.enabled = true;
        } 
    }
}
