﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeRoofObstructReset : MonoBehaviour {

    public FadeRoofObstruct fadeRoofObstruct;
    // Use this for initialization

    void OnTriggerEnter(Collider collider)
    {
        if(collider.tag == "Player")    
        {    
            fadeRoofObstruct.RestoreRoofMaterialAlpha();
        }
    }
}
