﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour {

    // Use this for initialization
    public Button playButton;
    public Button quitGameButton;
    void Start () {

        playButton.onClick.AddListener(StartScene);
        quitGameButton.onClick.AddListener(QuitGame);
        PlayerPrefs.SetString("Instructions", "");
    }

    void StartScene()
    {
        SceneManager.LoadScene("02.Level01",LoadSceneMode.Single);
    }

    void QuitGame()
    {
        Application.Quit ();
    }
}
