﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
public class ButtonControllerInGame : MonoBehaviour {

    // Use this for initialization
    //public GameObject escButton;
    public Button resumeButton;
    public Button restartButton;
    public Button quitGameButton;
    public Button yesButton;
    public Button noButton;
    public Button okButton;
    public GameObject pauseMenu;
    public GameObject exitQuestionMenu;
    public GameObject tryAgainMenu;
    public GameObject instructionsMenu;
    public GameObject playAgainMenu;
    public Button tryAgainYes;
    public Button tryAgainNo;
    public Button playAgainYes;
    public Button playAgainNo;
    public TextMeshProUGUI playAgainMenuText;
    //public AudioSource musicSource;
    private float currentVolume;
    //public PlayerController controllerStatus;
    private bool questionBeforeExit = false;
    private bool disableEscKey = true;
    private CanvasTimer canvasTimer;
    public AudioSource musicAudioSource;

    void Start ()
    {
        //Time.timeScale = 0f;
        resumeButton.onClick.AddListener(ResumeGame);
        quitGameButton.onClick.AddListener(QuestionBeforeExit);
        currentVolume = musicAudioSource.volume;
        noButton.onClick.AddListener(QuestionBeforeExit);
        yesButton.onClick.AddListener(QuitGame);
        restartButton.onClick.AddListener(RestartGame);
        tryAgainYes.onClick.AddListener(RestartGame);
        tryAgainNo.onClick.AddListener(TryAgainNo);
        okButton.onClick.AddListener(OkToStartGame);
        canvasTimer = GetComponent<CanvasTimer>();
        playAgainYes.onClick.AddListener(RestartGame);
        playAgainNo.onClick.AddListener(TryAgainNo);
        if (PlayerPrefs.GetString("Instructions") == "Readed")
        {
            OkToStartGame();
        }
    }

    // Update is called once per frame
    void Update ()
    {       
        if (!disableEscKey)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!questionBeforeExit) //Si no me encuentro en la pregunta "Are you sure?"
                {
                    pauseMenu.gameObject.SetActive(!pauseMenu.gameObject.activeSelf);
                    //controllerStatus.controllerOn = !controllerStatus.controllerOn;
                    TimeScaleAndSound();
                }
                else if(questionBeforeExit) // Si me encuentro en la pregunta "Are you sure?"
                {
                    QuestionBeforeExit();
                }
            }
        }

    }

    public void EnableEscKey()
    {
        disableEscKey = false;
    }

    void OkToStartGame()
    {
        //Time.timeScale = 1f;
        instructionsMenu.SetActive(false);
        PlayerPrefs.SetString("Instructions", "Readed"); //Agrego esto, para que cuando recargo la escena, arranque el juego de una sin instrucciones
    }

    void ResumeGame() //Botón Resume. Regresa al juego
    {
        pauseMenu.gameObject.SetActive(false);
        //controllerStatus.controllerOn = true;
        TimeScaleAndSound();
    }

    void QuestionBeforeExit() //Abre y Cierra ventana que pregunta si se desea salir del juego
    {
        exitQuestionMenu.gameObject.SetActive(!exitQuestionMenu.gameObject.activeSelf);
        questionBeforeExit = !questionBeforeExit;
    }

    void QuitGame()
    {
        Application.Quit();
    }

    void RestartGame()
    {
        Time.timeScale = 1f;
        Scene loadedLevel = SceneManager.GetActiveScene ();
        SceneManager.LoadScene (loadedLevel.buildIndex);
        //SceneManager.LoadScene("02.Level01",LoadSceneMode.Single);
        //OkToStartGame(); //Para no mostrar el menu de instrucciones de nuevo
    }

    void TimeScaleAndSound()
    {
        if (pauseMenu.gameObject.activeSelf)
        {
            Time.timeScale = 0f;
            musicAudioSource.volume = 0.2f;
        }
        else if (!pauseMenu.gameObject.activeSelf)
        {
            Time.timeScale = 1f;
            musicAudioSource.volume = currentVolume;
        }
    }

    public void TryAgainQuestionMenu()
    {
        tryAgainMenu.SetActive(true);
        disableEscKey = true;
    }

    public void PlayAgainQuestionMenu()
    {
        playAgainMenu.SetActive(true);
        playAgainMenuText.text = "You beat all Zombies in <color=#00FF5BFF><size=27>" + canvasTimer.timerCounterInteger.ToString() + "</size></color> seconds.<br>Do you want to improve your time?";
        disableEscKey = true;
    }

    void TryAgainNo()
    {
        SceneManager.LoadScene("01.StartScreen",LoadSceneMode.Single);
    }
}
